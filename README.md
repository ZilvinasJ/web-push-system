# Web Push System

Web push system (WPS) is an open source distributed software, which isolates overhead of web push back-end. In addition WPS provides admin UI to initiate push and view analytics.

### Currently no stable release is available. Master (production) branches are not ready to use. Take a look in a dev branches instead. 

## Installing

1. You need to run server nodes.
To do that download dispacther, public, private api sources. Then use docker-compose provided in main repository and execute this line of code ```docker-compose up --build redis public-api private-api dispatcher admin```
It will bring up redis, public, private, admin nodes. Admin node will be brought up using development create react app server. In production you MUST deploy it safely with minified files.
2. If you want to try web push with sample page - download client source. Run ```npm install``` and then ```npm run```
3. If you want to use web push in your own website go to client source public directory and copy **sw.js** and **sdk.js**. In your index.html add sdk script ```<script src="sdk.js"></script>``` and add ```<script src="http://cdn.jsdelivr.net/npm/pouchdb@7.0.0/dist/pouchdb.min.js"></script>``` Pouch db dependency will be deprecated in a future releases. Sw.js will be included by sdk.js

#### To initiate subscription have a look to sdk.js or in client repository README.md

## WPS consists of:
 - [Admin] 
 - [Client] 
 - [Dispatcher]
 - [Private-API]
 - [Public-API]

## Diagram:

![Topology diagram](https://bitbucket.org/ZilvinasJ/web-push-system/downloads/WPS%20architecture.png)

## Flows

#### Subscribing

1. Client subscribers to push service and receives subscription data.
2. Subscription data is passed to public API via HTTP POST /subscribers
3. Data is stored in Redis DB

#### Sending push notification

1. Via administrator UI notification information is filled and sent to private API via HTTP POST /notifications
2. Private API adds filtered subscribers ids into Redis set.
3. Private API publishes task to all dispatchers of the system.
4. All dispatchers from the Redis set pops subscribers and sends notifications in a parallel way.

##### Detail workflows are described in each README.md of subrepository

## Redis data schema in JSON

```
{
  "subscribers": {
    "history": "sorted set",
    "subscriptions": {
      "{id}": "string"
    },
    "metadata-index": {
      "integer": {
        "{id}": "hash"
      },
      "string": {
        "{id}": "hash"
      }
    },
    "metadata": {
      "id": "sorted set",
      "integer": {
        "{tag}": "sorted set"
      },
      "string": {
        "{tag}": "sorted set"
      }
    }
  },
  "notifications": {
    "{id}": {
      "content": "string",
      "analytics": {
        "clicks": "set",
        "impressions": "set",
        "expectedReceivers": "string",
        "actualReceivers": "string"
      },
      "metadata": "string"
    }
  },
  "tasks": {
    "{tid}": "set"
  },
  "settings": "hash"
}
```
   
   [Admin]: <https://bitbucket.org/ZilvinasJ/wps_admin/src/dev/README.md>
   [Client]: <https://bitbucket.org/ZilvinasJ/wps_client/src/dev/README.md>
   [Dispatcher]: <https://bitbucket.org/ZilvinasJ/wps_dispatcher/src/dev/README.md>
   [Private-API]: <https://bitbucket.org/ZilvinasJ/wps_private_api/src/dev/README.md>
   [Public-API]: <https://bitbucket.org/ZilvinasJ/wps_public_api/src/dev/README.md>
